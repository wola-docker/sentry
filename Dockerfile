FROM getsentry/sentry:23.5.2

RUN apt-get update && \
    apt-get install -y \
    gcc \
    libsasl2-dev \
    python-dev \
    libldap2-dev \
    libssl-dev && \
    apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

RUN pip install python-ldap sentry-ldap-auth
